EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JP8-4ALL-cache
LIBS:JP8-control-cache
LIBS:JP8-voice-cache
LIBS:JP8-voice-rescue
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 21 26
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4450 3450 875  1675
U 5C0275C5
F0 "vca-mod-mc" 60
F1 "ba662.sch" 60
F2 "1" I L 4450 3750 60 
F3 "2" I L 4450 3875 60 
F4 "3" I L 4450 4000 60 
F5 "4" I L 4450 4125 60 
F6 "5" I L 4450 4250 60 
F7 "6" I L 4450 4375 60 
F8 "7" I L 4450 4500 60 
F9 "8" I L 4450 4625 60 
F10 "9" I L 4450 4750 60 
$EndSheet
Text GLabel 4450 4125 0    60   Input ~ 0
gnd
Text GLabel 4450 4250 0    60   Input ~ 0
-15v
Text GLabel 4450 4750 0    60   Input ~ 0
+15v
Text HLabel 4450 3750 0    60   Input ~ 0
1
Text HLabel 4450 3875 0    60   Input ~ 0
2
Text HLabel 4450 4000 0    60   Input ~ 0
3
Text HLabel 4450 4375 0    60   Input ~ 0
6
Text HLabel 4450 4500 0    60   Input ~ 0
7
Text HLabel 4450 4625 0    60   Input ~ 0
8
$EndSCHEMATC
