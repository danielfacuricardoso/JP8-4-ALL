EESchema Schematic File Version 2
LIBS:JP8-voice-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JP8-voice-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 27 50
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X09 IC28
U 1 1 5BEEACD0
P 4175 3650
AR Path="/5BE55593/5BED5E59/5BE63338/5BEEACD0" Ref="IC28"  Part="1" 
AR Path="/5BE67497/5BE7F4A6/5BE63338/5BEEACD0" Ref="IC22"  Part="1" 
AR Path="/5BE55593/5BF37FF2/5BE63338/5BEEACD0" Ref="IC34"  Part="1" 
AR Path="/5BE53239/5BE8A818/5BE63338/5BEEACD0" Ref="IC20"  Part="1" 
AR Path="/5BE53459/5C07ABEC/5BE63338/5BEEACD0" Ref="IC26"  Part="1" 
AR Path="/5BE56DA5/5BE92902/5BE63338/5BEEACD0" Ref="IC25"  Part="1" 
AR Path="/5BE53679/5C0BB3F4/5BE63338/5BEEACD0" Ref="IC32"  Part="1" 
AR Path="/5BE53679/5C0BA3AD/5BE63338/5BEEACD0" Ref="IC31"  Part="1" 
AR Path="/5BE531DD/5C09C863/5BE63338/5BEEACD0" Ref="IC18"  Part="1" 
AR Path="/5C00211D/5C054554/5BE63338/5BEEACD0" Ref="IC8"  Part="1" 
AR Path="/5C009F11/5C054554/5BE63338/5BEEACD0" Ref="IC15"  Part="1" 
AR Path="/5BEC8CDD/5C027B43/5BEEACD0" Ref="IC15"  Part="1" 
AR Path="/5BEC8CDD/5C02754A/5C0275C5/5BEEACD0" Ref="IC15"  Part="1" 
AR Path="/5BEC93CC/5C034E66/5C0275C5/5BEEACD0" Ref="IC16"  Part="1" 
AR Path="/5BEC93CC/5C0352E9/5BE63338/5BEEACD0" Ref="IC17"  Part="1" 
AR Path="/5BEC8CDD/5C027B43/5BE63338/5BEEACD0" Ref="IC14"  Part="1" 
AR Path="/5BEC8CE1/5C02D70B/5BE63338/5BEEACD0" Ref="IC15"  Part="1" 
AR Path="/5BEC8CDF/5C030800/5C0275C5/5BEEACD0" Ref="IC12"  Part="1" 
AR Path="/5BEC93CE/5C04578B/5BE63338/5BEEACD0" Ref="IC8"  Part="1" 
F 0 "IC8" H 4175 4150 50  0000 C CNN
F 1 "BA662" V 4275 3650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x09_Pitch2.54mm" H 4175 3650 50  0001 C CNN
F 3 "" H 4175 3650 50  0001 C CNN
	1    4175 3650
	1    0    0    -1  
$EndComp
Text HLabel 3975 3250 0    60   Input ~ 0
1
Text HLabel 3975 3350 0    60   Input ~ 0
2
Text HLabel 3975 3450 0    60   Input ~ 0
3
Text HLabel 3975 3550 0    60   Input ~ 0
4
Text HLabel 3975 3650 0    60   Input ~ 0
5
Text HLabel 3975 3750 0    60   Input ~ 0
6
Text HLabel 3975 3850 0    60   Input ~ 0
7
Text HLabel 3975 3950 0    60   Input ~ 0
8
Text HLabel 3975 4050 0    60   Input ~ 0
9
$EndSCHEMATC
